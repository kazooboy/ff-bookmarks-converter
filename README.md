# Firefox Bookmarks Converter

# THIS WAS A TERRIBLE IDEA FOR MERCY'S SAKE PLEASE DO NOT USE THIS (unless you want to, it's Apache licensed so...)

Converts a .json backup file of Firefox bookmarks to nested folders containing an HTML file for each bookmark. Idea is to have a simple, portable, and private way to store bookmarks outside of any particular browser.

To use, run the script file with the bookmarks.json file as an argument, e.g.
`python3 convert-bookmarks.py bookmarks.json`

It probably also supports passing the JSON text as a pipe, but I haven't tried it.

In the current directory, it will hopefully create a folder called "bookmarks" (unless you've renamed the root node in the JSON), and will create subfolders mirroring the structure of your bookmarks. Each bookmark will get an HTML file that contains the link. Each HTML file also includes a paragraph tag set for you to add your own comments. 

Note that this script will automatically strip any long bookmark titles to conform to filesystem limits. It's not *guaranteed* that it will strip them short enough, but it will try.

If you have two bookmarks with the same name in the same folder, it will append a (hopefully) unique number to differentiate them.

This is licensed with the Apache 2.0 license, see the LICENSE file for details.

I've put quite a few days' worth of work into this, but you won't be able to tell. Expect bugs. Don't expect updates. 
