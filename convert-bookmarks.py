#!/usr/bin/env python3

# Copyright 2019 Kenton Tofte
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

    # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import fileinput
import html
import json
import os
import os.path
import re
import sys

def sanitize_filename(name):
    name = re.sub("^\s*","",name)
    name = re.sub("$\s*","",name)
    name = re.sub("[^a-zA-Z0-9 ]", "", name)
    return name[:64]

def generate_bookmark_file(path, title, link, unique_id):
    if title == "":
        title = str(unique_id)
    filename = sanitize_filename(title)
    path_filename = os.path.join(path, filename+".html")
    if os.path.exists(path_filename):
        path_filename = os.path.join(path, filename+"-"+str(unique_id)+".html")
        if os.path.exists(path_filename):
            print("Error: %s already exists" % path_filename)
    with open(path_filename, 'w', encoding='utf-8') as bookmark_file:
        bookmark_file.write('<!doctype html>\n')
        bookmark_file.write('<html lang="en">\n')
        bookmark_file.write('<meta charset="utf-8">\n')
        bookmark_file.write('<p><a href=' + link + '>' + html.escape(title) + '</a></p>\n')
        bookmark_file.write('<!--Feel free to use this next paragraph for comments:-->\n')
        bookmark_file.write('<p></p>\n')
        bookmark_file.write('</html>\n')

def generate_bookmarks(path, bookmarks):
    if bookmarks.get('type') == 'text/x-moz-place-container':
        folder_name = sanitize_filename(bookmarks.get('title'))
        if folder_name == "":
            folder_name = "bookmarks"
        sanitized_path = os.path.join(path,folder_name)
        if not os.path.exists(sanitized_path):
            os.mkdir(sanitized_path)
        if 'children' in bookmarks:
            for child in bookmarks.get('children'):
                generate_bookmarks(sanitized_path, child)
    if bookmarks.get('type') == 'text/x-moz-place':
        generate_bookmark_file(path, bookmarks.get('title'), bookmarks.get('uri'), bookmarks.get('id'))

with fileinput.input(openhook=fileinput.hook_encoded("utf-8")) as bookmarks_file:
    for line in bookmarks_file:
        generate_bookmarks(os.getcwd(), json.loads(line))
